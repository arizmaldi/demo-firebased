import { Injectable } from '@angular/core';

import { AngularFirestore } from '@angular/fire/firestore';
import { throwError } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class CrudService {
    constructor(
        private firestore: AngularFirestore
    ) { }


    create_NewStudent(record) {
        return this.firestore.collection('Students').add(record);
    }

    read_Students() {
        return this.firestore.collection('Students').snapshotChanges();
    }

    update_Student(recordID, record) {
        this.firestore.doc('Students/' + recordID).update(record).catch(this.handleError);
    }

    delete_Student(record_id) {
        this.firestore.doc('Students/' + record_id).delete().catch(this.handleError);
    }

    handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
          // client-side error
          errorMessage = `Error: ${error.error.message}`;
        } else {
          // server-side error
          errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        window.alert(errorMessage);
        return throwError(errorMessage);
      }
}