import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
// import { AngularFirestore } from '@angular/fire/firestore/firestore';
// import { BehaviorSubject } from 'rxjs';

// export const FirestoreStub = {
//   collection: (name: string) => ({
//       doc: (_id: string) => ({
//           valueChanges: () => new BehaviorSubject({ foo: 'bar' }),
//           set: (_d: any) => new Promise((resolve, _reject) => resolve()),
//       }),
//   }),
// };

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        FormsModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [{provide: AngularFirestore}]
    }).compileComponents();
  }));

  it('Should have as title', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('firebase-app')
  }))

});